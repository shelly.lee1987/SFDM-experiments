/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2012 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "palabos3D.h"
#include "palabos3D.hh"
//#include "cylinder.h"
#include "rhombusScreen.h"
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace plb;
using namespace plb::descriptors;
using namespace std;

typedef double T;
#define DESCRIPTOR descriptors::D3Q19Descriptor


/// A functional, used to initialize a pressure boundary to constant density
template<typename T>
class ConstantDensity {
public:
    ConstantDensity(T density_)
        : density(density_)
    { }
    T operator()(plint iX, plint iY, plint i) const {
        return density;
    }
private:
    T density;
};


/// A functional, used to instantiate bounce-back nodes at the locations of the cylinder
void defineCylinderGeometry( MultiBlockLattice3D<T,DESCRIPTOR>& lattice,
                             IncomprFlowParam<T> const& parameters,
                             OnLatticeBoundaryCondition3D<T,DESCRIPTOR>& boundaryCondition, Array<plint,3> forceIds)
{
    const plint nx = parameters.getNx();
    const plint ny = parameters.getNy();
    const plint nz = parameters.getNz();
    const plint N = parameters.getResolution();
    
    Box3D inlet(0,0, 0, ny-1, 0, nz-1);
    Box3D outlet(nx-1,nx-1, 0, ny-1, 0, nz-1);
    
    lattice.periodicity().toggleAll(true); // Use periodic boundaries.

    // Create Velocity boundary conditions for inlet
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, inlet);
    // right boundary, where we prefer a fixed-pressure condition.
    boundaryCondition.setPressureConditionOnBlockBoundaries(lattice, outlet);
   // boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, outlet);
   

    
    
    // All cells have initially density rho ... 
    T rho0=1.;
    T u = parameters.getLatticeU();
    T theta = 45.;
    T pi = acos(-1.);
    T ux = u * cos(theta*pi/180.);
    T uy = u * sin(theta*pi/180.);
    T uz = 0.;
    
    setBoundaryVelocity (
            lattice, inlet,
            Array<T,3>(ux,uy,uz) );
    setBoundaryDensity (
            lattice, outlet,
            ConstantDensity<T>(1.) );
    //setBoundaryVelocity (
      //      lattice, outlet,
        //    Array<T,2>(ux,uy) );
    
    initializeAtEquilibrium (
            lattice, lattice.getBoundingBox(),
            rho0, Array<T,3>(ux,uy,uz) );

    int cx = nx/2;
    int cy = ny/2;
    int cz = nz/2;
    int radius = 2;
    // Instead of plain BounceBack, use the dynamics MomentumExchangeBounceBack,
    //   to compute the momentum exchange, and thus, the drag and the lift on
    //   the obstacle, locally during collision.

    
    defineDynamics(lattice, lattice.getBoundingBox(),
                   new RhombusScreenFourEdges<T>(cx,N,radius),
                   new MomentumExchangeBounceBack<T,DESCRIPTOR>(forceIds));
    
     initializeMomentumExchange(lattice, lattice.getBoundingBox(), new RhombusScreenFourEdges<T>(cx, N, radius) );
    
    lattice.initialize();
}


void writeVTK(MultiBlockLattice3D<T,DESCRIPTOR>& lattice,
              IncomprFlowParam<T> const& parameters, plint iter)
{
    T dx = parameters.getDeltaX();
    T dt = parameters.getDeltaT();
    VtkImageOutput3D<T> vtkOut(createFileName("vtk", iter, 8), dx);
    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", dx/dt);
    vtkOut.writeData<3,float>(*computeVelocity(lattice), "velocity", dx/dt);
}

void writeTextFile(MultiBlockLattice3D<T,DESCRIPTOR>& lattice, plint iter) {
    plb_ofstream pfile((createFileName("pressure", iter, 8)+".txt").c_str());
    pfile << *multiply(1./3., *add(-1.0, *computeDensity(lattice)));
    
    plb_ofstream ufile((createFileName("velocity", iter, 8)+".txt").c_str());
    ufile << *computeVelocity(lattice);

}

int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");

    IncomprFlowParam<T> parameters(
            (T) 5e-4,  // uMax
            (T) 5,  // Re
            50,        // N
            10.,         // lx
            1.,         // ly 
            1.          //lz
    );
        
    const T logT     = (T)0.005;
    const T vtkSave  = (T)1.;
    const T textSave = (T)50.;
    const T maxT     = (T)15.;

    writeLogFile(parameters, "Poiseuille flow");

    MultiBlockLattice3D<T, DESCRIPTOR> lattice (
            parameters.getNx(), parameters.getNy(), parameters.getNz(),
            new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    lattice.initialize();

    // The drag and lift acting on the obstacle are computed with help of the
    //   internal statistics object of the lattice. For this purpose, they
    //   need to be registered first, as it is done in the following lines.
    Array<plint,3> forceIds;
    forceIds[0] = lattice.internalStatSubscription().subscribeSum();
    forceIds[1] = lattice.internalStatSubscription().subscribeSum();
    forceIds[2] = lattice.internalStatSubscription().subscribeSum();



    OnLatticeBoundaryCondition3D<T,DESCRIPTOR>*
        boundaryCondition = createLocalBoundaryCondition3D<T,DESCRIPTOR>();

    defineCylinderGeometry(lattice, parameters, *boundaryCondition, forceIds);

    // Main loop over time iterations.
    
    FILE *fp_force = NULL;
    
    if (global::mpi().isMainProcessor()) {
      fp_force = fopen("force.dat","w+");
      PLB_ASSERT(fp_force!=NULL);
    }
    
    
    T aforce=0.;
    T bforce=0.;
    plint iT=0;
    //for (; iT*parameters.getDeltaT()<maxT; ++iT) {
    do{
     
        //global::timer("mainLoop").restart();
        
        if (iT%parameters.nStep(vtkSave)==0 && iT>0) {
            pcout << "Saving VTK file ..." << endl;
            writeVTK(lattice, parameters, iT);
        }

        if (iT%parameters.nStep(textSave)==0 && iT>0) {
            pcout << "Saving text file ..." << endl;
	    writeTextFile(lattice, iT);
        }

        if (iT%parameters.nStep(logT)==0) {
	    aforce= bforce; 
            pcout << "step " << iT
                  << "; lattice time=" << lattice.getTimeCounter().getTime()
                  << "; t=" << iT*parameters.getDeltaT();
        }

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();

        if (iT%parameters.nStep(logT)==0) {
	    
            pcout << "; av energy="
                  << setprecision(10) << getStoredAverageEnergy<T>(lattice)
                  << "; av rho="
                  << getStoredAverageDensity<T>(lattice)
                  << endl;
		  
		  if(global::mpi().isMainProcessor()){
                   fprintf(fp_force, "% .8e\t% .8e\t% .8e\t% .8e\n", 
	                          (T) iT*parameters.getDeltaT(), 
			          (T) lattice.getInternalStatistics().getSum(forceIds[0]), 
			          (T) lattice.getInternalStatistics().getSum(forceIds[1]), 
			          (T) lattice.getInternalStatistics().getSum(forceIds[2]));			
                   fflush(fp_force);
		}
				
	bforce=lattice.getInternalStatistics().getSum(forceIds[0]); 
		
        }  
        
   
    ++iT;
    }while( iT*parameters.getDeltaT()<maxT || fabs((bforce-aforce)/bforce)>=1e-8);
   
    
    writeTextFile(lattice, iT); 
    pcout << "Saving Pressure file ..." << endl;
    pcout << "Saving Velocity file ..." << endl;
    pcout << "Saving The Strain Rate From Stress file ..." << endl;
    	   

    delete boundaryCondition;
}
