/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2012 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RHOMBUSSCREEN_H
#define RHOMBUSSCREENH

#include "palabos3D.h"
template<typename T>
class RhombusScreenTwoEdges : public plb::DomainFunctional3D {
public:
    RhombusScreenTwoEdges(plb::plint cx_, plb::plint N_, plb::plint radius)
        : cx(cx_),
          N(N_),
          radiusSqr(plb::util::sqr(radius))
    { }

    
    virtual bool operator()(plb::plint iX, plb::plint iY, plb::plint iZ) const{
        plb::plint cz1;
        plb::plint cz2;
        
        
        cz1 = iY;
        cz2 = -iY + N;
        
        if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz1) <= radiusSqr)
            return true;
        else{
             if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz2) <= radiusSqr)
                 return true;
             else
                 return false;
        }
    }
    virtual RhombusScreenTwoEdges<T>* clone() const {
        return new RhombusScreenTwoEdges<T>(*this);
    }
private:
    plb::plint cx;
    plb::plint N;
    plb::plint radiusSqr;
};
//////////////////////////////////////////////////////////////////////////////////////////
template<typename T>
class RhombusScreenFourEdges : public plb::DomainFunctional3D {
public:
    RhombusScreenFourEdges(plb::plint cx_, plb::plint N_, plb::plint radius)
        : cx(cx_),
          N(N_),
          radiusSqr(plb::util::sqr(radius))
    { }

    
    virtual bool operator()(plb::plint iX, plb::plint iY, plb::plint iZ) const{
        plb::plint cz1;
        plb::plint cz2;
        plb::plint cz3;
        plb::plint cz4;
        
        
        cz1 = (plb::plint)(iY/2.+0.5);
        cz2 = (plb::plint)((iY+N)/(2.)+0.5);
        
        cz3 = (plb::plint)((N-iY)/(2.)+0.5);
        cz4 = (plb::plint)((2.*N-iY)/(2.)+0.5);
        
        
        if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz1) <= radiusSqr)
            return true;
        else{
             if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz2) <= radiusSqr)
                 return true;
             else{
                 if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz3) <= radiusSqr)
                     return true;
                 else{
                     if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz4) <= radiusSqr)
                        return true;
                     else
                         return false;
                }
            }
             
        }
    }
    virtual RhombusScreenFourEdges<T>* clone() const {
        return new RhombusScreenFourEdges<T>(*this);
    }
private:
    plb::plint cx;
    plb::plint N;
    plb::plint radiusSqr;
};
/////////////////////////////////////////////////////////////////////////////////////
template<typename T>
class RhombusScreenSixEdges : public plb::DomainFunctional3D {
public:
    RhombusScreenSixEdges(plb::plint cx_, plb::plint N_, plb::plint radius)
        : cx(cx_),
          N(N_),
          radiusSqr(plb::util::sqr(radius))
    { }

    
    virtual bool operator()(plb::plint iX, plb::plint iY, plb::plint iZ) const{
        plb::plint cz1;
        plb::plint cz2;
        plb::plint cz3;
        plb::plint cz4;
        plb::plint cz5;
        plb::plint cz6;
        
        
        cz1 = (plb::plint)(iY/3.+0.5);
        cz2 = (plb::plint)((iY+N)/(3.)+0.5);
        cz3 = (plb::plint)((iY+2.*N)/(3.)+0.5);
  
        cz4 = (plb::plint)((N-iY)/(3.)+0.5);
        cz5 = (plb::plint)((2.*N-iY)/(3.)+0.5);
        cz6 = (plb::plint)((3.*N-iY)/(3.)+0.5);
        
        
        if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz1) <= radiusSqr)
            return true;
        else{
             if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz2) <= radiusSqr)
                 return true;
             else{
                 if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz3) <= radiusSqr)
                     return true;
                 else{
                     if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz4) <= radiusSqr)
                        return true;
                     else{
                         if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz5) <= radiusSqr)
                             return true;
                         else{
                             if(plb::util::sqr(iX-cx) + plb::util::sqr(iZ-cz6) <= radiusSqr)
                                 return true;
                             else
                                 return false;
                        }
                    }
                }
            }
             
        }
    }
    virtual RhombusScreenSixEdges<T>* clone() const {
        return new RhombusScreenSixEdges<T>(*this);
    }
private:
    plb::plint cx;
    plb::plint N;
    plb::plint radiusSqr;
};



#endif  // CYLINDER_H
