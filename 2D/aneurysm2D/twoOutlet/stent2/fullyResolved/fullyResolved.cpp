/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2012 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "palabos2D.h"
#include "palabos2D.hh"
#include "poiseuille.h"
#include "poiseuille.hh"
#include "multicylinder.h"
#include "multicylinder.hh"
#include "microstent_util.h"
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace plb;
using namespace plb::descriptors;
using namespace std;

typedef double T;
//#define DESCRIPTOR D2Q9Descriptor
#define DESCRIPTOR ForcedD2Q9Descriptor

const T l = 1.0;
const T Part1_tube = 8.0;
const T Part2_tube = 6.0;
const T Part3_R1 = 2.0;
const T Part3_R2 = 3.0;
const T Part3_Cx = Part2_tube + l + Part3_R1;
const T Part3_Cy = Part1_tube;
const T Part3_tube = 4.0;
const T aneurysmRadius = 1.0;
const T aneurysmCx = Part2_tube - 0.4;
const T aneurysmCy = Part1_tube + l + 0.8; 

// const T stentR = Part3_R2;
// const T stentCx = Part3_Cx;
// const T stentCy = Part3_Cy;
// const T stentHeight = 2.0;

const T stentR = 2.;
const T stentCx = Part2_tube + l - stentR;
const T stentCy = Part1_tube + l - stentR;
const T cornerR = stentR - l;

const T lx = Part2_tube + l + Part3_R1 + Part3_tube;
const T ly = Part1_tube + Part3_R2 + 0.1;

T dis, radius, n_ob;
T pi, dTheta;
T dx = 0.;

/// A functional, used to initialize a pressure boundary to constant density
template<typename T>
class ConstantDensity {
public:
    ConstantDensity(T density_)
        : density(density_)
    { }
    T operator()(plint iX, plint iY) const {
        return density;
    }
private:
    T density;
};



//////////////////////////////////////////////////////////////////////////////////////////////////////
/// boundary
void defineBoundary( MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
                             IncomprFlowParam<T> const& parameters,
                             OnLatticeBoundaryCondition2D<T,DESCRIPTOR>& boundaryCondition,
                             vector<Array<plint,2> > forceIds)
{    
    const plint nx = parameters.getNx();
    const plint ny = parameters.getNy();
    
    //inlet
    plint inletX0 = (plint)((Part2_tube) / dx + 0.5);
    plint inletX1 = (plint)((Part2_tube + l) / dx + 0.5);
    Box2D inlet(inletX0, inletX1, 0, 0);
    //outletA
    plint outletAY0 = (plint)((Part1_tube)/dx + 0.5);
    plint outletAY1 = (plint)((Part1_tube + l)/dx + 0.5);
    Box2D outletA(0, 0, outletAY0, outletAY1);
    //outletB
    plint outletBX = nx - 1;
    plint outletBY0 = (plint)((Part1_tube + Part3_R1)/dx + 0.5);
    plint outletBY1 = (plint)((Part1_tube + Part3_R2)/dx + 0.5);
    Box2D outletB(outletBX, outletBX, outletBY0, outletBY1);


    // Create Velocity boundary conditions on inlet Box
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, inlet);
    // .. except on right boundary, where we prefer a fixed-pressure condition.
    boundaryCondition.setPressureConditionOnBlockBoundaries(lattice, outletA);
//     boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, outlet);
    boundaryCondition.setPressureConditionOnBlockBoundaries(lattice, outletB);

  
    setBoundaryVelocity (
            lattice, inlet,
             PoiseuilleVelocity<T>(parameters, inletX0, inletX1, 0) );
//             Array<T,2>(0.,parameters.getLatticeU()) );
    
    setBoundaryDensity (
            lattice, outletA,
            ConstantDensity<T>(1.) );
    
    setBoundaryDensity (
            lattice, outletB,
            ConstantDensity<T>(1.) );
    
    
    initializeAtEquilibrium (
            lattice, lattice.getBoundingBox(),
            1., Array<T,2>(0., 0.) );  
    
    //stent
    plint stentRLb = (plint)(stentR / dx);
    plint stentCxLb = (plint)(stentCx / dx + 0.5);
    plint stentCyLb = (plint)(stentCy / dx + 0.5);
    vector<plint> cy(n_ob);
    vector<plint> cx(n_ob);
    
    for (plint i = 0; i<n_ob; i++)
      { 
        cx[i] = stentCxLb + (plint)(cos(dTheta*i) * stentRLb);
        cy[i] = stentCyLb + (plint)(sin(dTheta*i) * stentRLb);
      }
         
    // Instead of plain BounceBack, use the dynamics MomentumExchangeBounceBack,
    //   to compute the momentum exchange, and thus, the drag and the lift on
    //   the obstacle, locally during collision.        
     
     for (plint i=0; i<n_ob; i++) {
	 defineDynamics(lattice, lattice.getBoundingBox(),
                   new AngleCylinderShapeDomain2D<T>(cx,cy,radius, i),
                   new MomentumExchangeBounceBack<T,DESCRIPTOR>(forceIds[i]));

         initializeMomentumExchange(lattice, lattice.getBoundingBox(), new AngleCylinderShapeDomain2D<T>(cx, cy, radius, i) );

     }

    
    lattice.initialize();
}

//==========================================================================================
// The function used for set the values of positons and normals for the calculation of wss
void wallPosition(plint cx_,plint cy_,T radius_,std::vector<Array<T,2> >& positions,
                  std::vector<Array<T,2> >& normals)
{

    plint cx(cx_);
    plint cy(cy_);
    T radius(radius_ - 4.);
    Array<T,2> position, normal;
    plint iX, iY1, iY2, cyLow;
    T sinValue, cosValue;
    cyLow = (plint)((Part1_tube + l)/dx + 0.5) - 4; 
       
    for(iX=cx-radius; iX<=cx+radius; iX++){
        iY1 = (plint)((sqrt(radius*radius - (iX-cx)*(iX-cx))/4.)) * 4 + cy;
        iY2 = (plint)((sqrt(radius*radius - (iX-cx)*(iX-cx))/4.)) * 4 * (-1) + cy;
        
        position = {iX,iY1};
        positions.push_back(position);        
        sinValue = (T(iY1-cy))/radius;
        cosValue = (T(iX-cx))/radius;
        normal = {cosValue,sinValue};
        normals.push_back(normal); 
       

        if(iY2 >= cyLow){
           position = {iX,iY2};
           positions.push_back(position);        
           sinValue = (T(iY2-cy))/radius;
           cosValue = (T(iX-cx))/radius;
           normal = {cosValue,sinValue};
           normals.push_back(normal);     
        }                                
    }
    
    
}

    
void writeGifs(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter)
{
    const plint imSize = 600;

    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledGif(createFileName("u", iter, 8),
                               *computeVelocityNorm(lattice),
                               imSize, imSize );
}

void writeVTK(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
              IncomprFlowParam<T> const& parameters, plint iter)
{
    T dx = parameters.getDeltaX();
    T dt = parameters.getDeltaT();
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 8), dx);
    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", dx/dt);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", dx/dt);
}

void writeTextFile(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter) {
    plb_ofstream pfile((createFileName("pressure", iter, 8)+".txt").c_str());
    pfile << *multiply(1./3., *add(-1.0, *computeDensity(lattice)));
    
    plb_ofstream ufile((createFileName("velocity", iter, 8)+".txt").c_str());
    ufile << *computeVelocity(lattice);
        
    plb_ofstream sfile((createFileName("stress", iter, 8)+".txt").c_str());
    sfile << *computeStrainRateFromStress(lattice);
}

void wssFile(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,std::vector<Array<T,2> >& positions,
             std::vector<Array<T,2> >& normals, plint iter){
    
     vector<T> wssMeasure;
     wssMeasure = wssSingleProbes(lattice, positions, normals);
     plb_ofstream wfile((createFileName("wss", iter, 8)+".txt").c_str());
     for (pluint i=0; i<positions.size(); ++i) {
          wfile << i << "\t" << wssMeasure[i] << endl;
     }
}                     


int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");

    IncomprFlowParam<T> parameters(
            (T) 0.0137,  // uMax
            (T) 400.,  // Re
            360,        // N
	    lx, ly );

    const T logT     = (T)0.1;
    const T imSave   = (T)0.5;
    const T vtkSave  = (T)50;
    const T textSave = (T)100.;
    const T maxT     = (T)600.;
    const plint checkPointIter = parameters.nStep(100.);
    plint iniT, endT;
    pcout << "the maximux iteration is: " << parameters.nStep(maxT) << endl;

    if (argc != 3) {
        pcout << "Error; the syntax is \"" << argv[0] << " start-iter end-iter\"," << endl;
        return -1;
    }

    stringstream iniTstr, endTstr;
    iniTstr << argv[1]; iniTstr >> iniT;
    endTstr << argv[2]; endTstr >> endT;

    writeLogFile(parameters, "Poiseuille flow");
    ///*******************************************************************************************
    plint nx = parameters.getNx();
    plint ny = parameters.getNy();
    MultiBlockLattice2D<T, DESCRIPTOR> lattice (
            nx, ny, new BounceBack<T,DESCRIPTOR> );
    dx = parameters.getDeltaX();
    
    //Part1
    plint X0 = 0;
    plint X1 = (plint)((Part2_tube + l)/dx + 0.5);
    plint Y0 = (plint)((Part1_tube)/dx + 0.5);
    plint Y1 = (plint)((Part1_tube + l)/dx + 0.5);
    Box2D part1(X0, X1, Y0, Y1);
    defineDynamics(lattice, part1,
               new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    
    //Part2
    X0 = (plint)((Part2_tube)/dx + 0.5);
    X1 = (plint)((Part2_tube + l)/dx + 0.5);
    Y0 = 0;
    Y1 = (plint)((Part1_tube + l)/dx + 0.5);
    Box2D part2(X0, X1, Y0, Y1);
    defineDynamics(lattice, part2,
               new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    //Part3
    //Part3_circle
    plint CxLb = (plint)(Part3_Cx/dx + 0.5);
    plint CyLb = (plint)(Part3_Cy/dx + 0.5);
    plint Part3_R1Lb = (plint)(Part3_R1/dx + 0.5);
    plint Part3_R2Lb = (plint)(Part3_R2/dx + 0.5);
    
    defineDynamics(lattice, lattice.getBoundingBox(),
               new QuarterCircleLeftDomain2D<T>( CxLb, CyLb, Part3_R2Lb),
               new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    defineDynamics(lattice, lattice.getBoundingBox(),
               new QuarterCircleLeftDomain2D<T>( CxLb, CyLb, Part3_R1Lb),
               new BounceBack<T,DESCRIPTOR> );
    //Part3_tube
    X0 = (plint)(Part3_Cx/dx);
    X1 = nx;
    Y0 = (plint)((Part3_Cy+Part3_R1)/dx + 0.5);
    Y1 = (plint)((Part3_Cy+Part3_R2)/dx + 0.5);
    Box2D part3Tube(X0, X1, Y0, Y1);
    defineDynamics(lattice, part3Tube,
               new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    
     //corner
    //corner_square
    X0 = (plint)(stentCx/dx);
    X1 = (plint)((Part2_tube + l)/dx);
    Y0 = (plint)((stentCy)/dx + 0.5);
    Y1 = (plint)((Part1_tube + l)/dx);
    Box2D cornerSquare(X0, X1, Y0, Y1);
    defineDynamics(lattice, cornerSquare,
               // new NaiveExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               //new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    //corner circle
    CxLb = (plint)(stentCx/dx + 0.5);
    CyLb = (plint)(stentCy/dx + 0.5);
    plint cornerRLb = (plint)(cornerR/dx);   
    defineDynamics(lattice, lattice.getBoundingBox(),
               new QuarterCircleRightDomain2D<T>( CxLb, CyLb, cornerRLb),
               new BounceBack<T,DESCRIPTOR> );


    //Aneurysm
    plint aneurysmCxLb = (plint)(aneurysmCx / parameters.getDeltaX() + 0.5);
    plint aneurysmCyLb = (plint)(aneurysmCy / parameters.getDeltaX() + 0.5);
    plint aneurysmRadiusLb = (plint)(aneurysmRadius / parameters.getDeltaX() + 0.5);
    
    std::vector<plb::plint> cxlist, cylist;
    cxlist.push_back(aneurysmCxLb);
    cylist.push_back(aneurysmCyLb);
    defineDynamics(lattice, lattice.getBoundingBox(),
               //new HalfCircleDomain2D<T>( aneurysmCxLb,aneurysmCyLb, aneurysmRadiusLb),
               new AngleCylinderShapeDomain2D<T>(cxlist,cylist,aneurysmRadiusLb, 0),
               new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    
    //parameters for stent
    dis = 20;
    radius = 2;
    plint stentRLb = (plint)(stentR/ dx);
    dTheta = dis / stentRLb;
//     T total_theta = asin(stentHeight/stentR);
//     n_ob = (plint)(total_theta * stentRLb / dis + 0.5) + 1;  
    pi = acos(-1.);
    n_ob = (plint)(90. / 180. * pi * stentRLb / dis + 0.5) + 1;
    
    std::vector<Array<plint,2> > forceIds;
    for (int i=0; i<n_ob; ++i) {
        Array<plint,2> id;
        id[0] = lattice.internalStatSubscription().subscribeSum();
        id[1] = lattice.internalStatSubscription().subscribeSum();
        forceIds.push_back(id);
    }


   
    lattice.initialize();
///*******************************************************************************************
     
//     // The drag and lift acting on the obstacle are computed with help of the
//     //   internal statistics object of the lattice. For this purpose, they
//     //   need to be registered first, as it is done in the following lines. 
    OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
        boundaryCondition = createInterpBoundaryCondition2D<T,DESCRIPTOR>();
        //boundaryCondition = createLocalBoundaryCondition2D<T,DESCRIPTOR>();

    defineBoundary(lattice, parameters, *boundaryCondition, forceIds);   
//***********************************************************************************************
    //set the postion of the wall on which the wss would be measured
    vector<Array<T,2> > positions, normals;
    wallPosition(aneurysmCxLb,aneurysmCyLb,aneurysmRadiusLb,positions,normals);
    
//    //****************************************************************************//
//        //spongeZone
//    Array<plint,4> numSpongeCells;
//    numSpongeCells[0] = 0;
//    numSpongeCells[1] = 0;
//    numSpongeCells[2] = ny/4;
//    numSpongeCells[3] = 0;
//    Array<T,4> translationParameters(0.5, 0.5, 0.5, 0.5);
//    Array<T,4> scaleParameters(0.12, 0.12, 0.12, 0.12);
//    std::vector<MultiBlock2D *> args;
//    args.push_back(&lattice);
////     plint box_x0 = (plint)((margin+R2+R1)/dx);
//    plint box_x0 = (plint)((margin+R2)/dx);
//    plint box_x1 = (plint)((margin+2*R2)/dx+1.0);
//    plint box_y0 = 0;
//    plint box_y1 = ny-1;
////     applyProcessingFunctional (
////             new ViscositySpongeZone2D<T,DESCRIPTOR> (
////                nx, ny, parameters.getOmega(), numSpongeCells, translationParameters, scaleParameters ),
////             Box2D(box_x0, box_x1, box_y0, box_y1), args );
//    applyProcessingFunctional (
//            new ViscositySpongeZone2D<T,DESCRIPTOR> (
//               nx, ny, parameters.getOmega(), numSpongeCells, translationParameters, scaleParameters ),
//               rightTube, args );
//    
//    
    // Main loop over time iterations.    
    
    if (iniT>0) {
        //loadRawMultiBlock(lattice, "checkpoint.dat");
        loadBinaryBlock(lattice, "checkpoint.dat");
    }
    
    FILE *fp_force0 = NULL;

    if (global::mpi().isMainProcessor()) {
      fp_force0 = fopen("force0.dat","a");
      PLB_ASSERT(fp_force0!=NULL);
    }
    
    global::PlbTimer time;
    time.start();
    plint iT=0;
    //for (; iT*parameters.getDeltaT()<maxT; ++iT) {  
    for (iT=iniT; iT<endT; ++iT) {
      
//       if (iT%parameters.nStep(imSave)==0) {
//           pcout << "Saving Gif ..." << endl;
//           writeGifs(lattice, iT);
//       }
       
        if (iT%parameters.nStep(vtkSave)==0 && iT>0) {
            pcout << "Saving VTK file ..." << endl;
            writeVTK(lattice, parameters, iT);
        }
        
             
        if (iT*parameters.getDeltaT()>=20 && iT%parameters.nStep(textSave)==0) {
            pcout << "Saving text file ..." << endl;
	    writeTextFile(lattice, iT);
            wssFile(lattice, positions, normals, iT);
	    
	    plb_ofstream fafile((createFileName("forceAll", iT, 8)+".txt").c_str());    
            for(int i =0;i<n_ob;i++){
               fafile << (T) lattice.getInternalStatistics().getSum(forceIds[i][0])  << "\t"
             << (T) lattice.getInternalStatistics().getSum(forceIds[i][1]);
	    }
        }
        
        if (iT%checkPointIter==0 && iT>iniT) {
            pcout << "Saving the state of the simulation ..." << endl;
            //saveRawMultiBlock(lattice, "checkpoint.dat");
            saveBinaryBlock(lattice, "checkpoint.dat");
        }
        
        if (iT%parameters.nStep(logT)==0) {
            pcout << "step " << iT
                  << "; lattice time=" << lattice.getTimeCounter().getTime()
                  << "; t=" << iT*parameters.getDeltaT()
                  << "; time=" << time.getTime(); ;
        }

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();

        if (iT%parameters.nStep(logT)==0) {
            plint mid = (plint)(n_ob/2);
            pcout << "; av energy="
                  << setprecision(10) << getStoredAverageEnergy<T>(lattice)
                  << "; av rho="
                  << getStoredAverageDensity<T>(lattice)
                  << "; drag=" << lattice.getInternalStatistics().getSum(forceIds[mid][0])
                  << "; lift=" << lattice.getInternalStatistics().getSum(forceIds[mid][1])
                  << endl;           
        

            if(global::mpi().isMainProcessor()){
                  fprintf(fp_force0, "% .8e\t% .8e\t% .8e\n", 
	          (T) iT*parameters.getDeltaT(), 
	          (T) lattice.getInternalStatistics().getSum(forceIds[mid][0]), 
		  (T) lattice.getInternalStatistics().getSum(forceIds[mid][1]));			    
                  fflush(fp_force0);
            }	 

        }
        
    }
    
    writeTextFile(lattice, iT); 
    wssFile(lattice, positions, normals, iT);
    pcout << "Saving Pressure file ..." << endl;
    pcout << "Saving Velocity file ..." << endl;
    pcout << "Saving The Strain Rate From Stress file ..." << endl;
    pcout << "Saving The Wss file ..." << endl;
    
    FILE *fp_forceAll = NULL;
    if (global::mpi().isMainProcessor()) {
        fp_forceAll = fopen("forceAll.dat","w+");
        PLB_ASSERT(fp_forceAll!=NULL);
	for(int i =0;i<n_ob;i++){
	   fprintf(fp_forceAll, "% .8e\t% .8e\n", 
	            (T) lattice.getInternalStatistics().getSum(forceIds[i][0]), 
                    (T) lattice.getInternalStatistics().getSum(forceIds[i][1]));			    ;
           fflush(fp_forceAll);
        }
     }


    delete boundaryCondition;
}
