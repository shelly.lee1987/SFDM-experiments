#include "atomicBlock/dataProcessingFunctional2D.h"
#include "atomicBlock/reductiveDataProcessingFunctional2D.h"

namespace plb {

template<typename T, template<typename U> class Descriptor> 
class WssSingleProbe2D :
    public ReductiveBoxProcessingFunctional2D_L<T,Descriptor>
{
public:
    WssSingleProbe2D(
            std::vector<Array<T,2> > const& positions_,
            std::vector<Array<T,2> > const& normals_);
    virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice);
    virtual WssSingleProbe2D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::nothing;
    }
    std::vector<T> getWss() const;
private:
    std::vector<Array<T,2> > positions;
    std::vector<Array<T,2> > normals;
    std::vector<plint> wssIds;
};

/* ******** WssSingleProbe2D *********************************** */

template<typename T, template<typename U> class Descriptor>
WssSingleProbe2D<T,Descriptor>::WssSingleProbe2D (
        std::vector<Array<T,2> > const& positions_,
        std::vector<Array<T,2> > const& normals_ )
    : positions(positions_),
      normals(normals_)
{
    PLB_ASSERT( positions.size() == normals.size() );
    wssIds.resize(positions.size());
    for (pluint iWss=0; iWss<positions.size(); ++iWss) {
        wssIds[iWss] = this->getStatistics().subscribeSum();
    }
}

template<typename T, template<typename U> class Descriptor>
void WssSingleProbe2D<T,Descriptor>::process (
        Box2D domain, BlockLattice2D<T,Descriptor>& lattice )
{
    std::vector<Dot2D> cellPos(4);
    std::vector<T> weights(4);
    T wss;
    for (pluint iWss=0; iWss<positions.size(); ++iWss) {
        wss = 0.;
        Array<T,2> position(positions[iWss]);
        Array<T,2> normal(normals[iWss]);
        Dot2D referenceCellPos((plint)position[0], (plint)position[1]);
        referenceCellPos -= lattice.getLocation();
        if (contained(referenceCellPos, domain)) {
            linearInterpolationCoefficients(lattice, position, cellPos, weights);
            for (plint iCell=0; iCell<4; ++iCell) {
                Cell<T,Descriptor> const& cell = lattice.get(cellPos[iCell].x,cellPos[iCell].y);

                T rhoBar;
                Array<T,2> j;
                Array<T,SymmetricTensorImpl<T,2>::n> PiNeq;
                cell.getDynamics().computeRhoBarJPiNeq(cell, rhoBar, j, PiNeq);
                T rho = Descriptor<T>::fullRho(rhoBar);
                Array<T,2> Pi_n;
                SymmetricTensorImpl<T,2>::matVectMult(PiNeq, normal, Pi_n);

                T omega = cell.getDynamics().getOmega();
                Array<T,2> forceOnWall = (rho-1.)*Descriptor<T>::cs2*normal - (omega/2.-1.)*Pi_n; 

                T newWss = norm(forceOnWall - dot(normal,forceOnWall)*normal);
                wss += weights[iCell]*newWss;

            }
        }
       this->getStatistics().gatherSum(wssIds[iWss], wss);
    }
}

template<typename T, template<typename U> class Descriptor>
WssSingleProbe2D<T,Descriptor>*
    WssSingleProbe2D<T,Descriptor>::clone() const
{
    return new WssSingleProbe2D<T,Descriptor>(*this);
}

template<typename T, template<typename U> class Descriptor>
std::vector<T> WssSingleProbe2D<T,Descriptor>::getWss() const {
    std::vector<T> wssValues(positions.size());
    for (pluint iWss=0; iWss<positions.size(); ++iWss) {
        wssValues[iWss] = this->getStatistics().getSum(wssIds[iWss]);
    }
    return wssValues;
}


template<typename T, template<typename U> class Descriptor>
std::vector<T> wssSingleProbes (
        MultiBlockLattice2D<T,Descriptor>& lattice, Box2D domain,
        std::vector<Array<T,2> > const& positions,
        std::vector<Array<T,2> > const& normals )
{
    WssSingleProbe2D<T,Descriptor> functional(positions, normals);
    applyProcessingFunctional(functional, domain, lattice);
    return functional.getWss();
}

template<typename T, template<typename U> class Descriptor>
std::vector<T> wssSingleProbes (
        MultiBlockLattice2D<T,Descriptor>& lattice,
        std::vector<Array<T,2> > const& positions,
        std::vector<Array<T,2> > const& normals )
{
    return wssSingleProbes(lattice, lattice.getBoundingBox(), positions, normals);
}

} // namespace plb


    



