/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2012 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "palabos2D.h"
#include "palabos2D.hh"
#include "poiseuille.h"
#include "poiseuille.hh"
#include "multicylinder.h"
#include "multicylinder.hh"
#include "microstent_util.h"
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace plb;
using namespace plb::descriptors;
using namespace std;

typedef double T;
//#define DESCRIPTOR D2Q9Descriptor
#define DESCRIPTOR ForcedD2Q9Descriptor

const T l = 1.0;
const T Part1_tube = 8.0;
const T Part2_tube = 6.0;
const T Part3_R1 = 2.0;
const T Part3_R2 = 3.0;
const T Part3_Cx = Part2_tube + l + Part3_R1;
const T Part3_Cy = Part1_tube;
const T Part3_tube = 4.0;
const T aneurysmRadius = 1.0;
const T aneurysmCx = Part2_tube - 0.4;
const T aneurysmCy = Part1_tube + l + 0.8; 

const T stentR = Part3_R2;
const T stentCx = Part3_Cx;
const T stentCy = Part3_Cy;
const T stentHeight = 2.0;


const T lx = Part2_tube + l + Part3_R1 + Part3_tube;
const T ly = Part1_tube + Part3_R2 + 0.1;


T dx = 0.;

/// A functional, used to initialize a pressure boundary to constant density
template<typename T>
class ConstantDensity {
public:
    ConstantDensity(T density_)
        : density(density_)
    { }
    T operator()(plint iX, plint iY) const {
        return density;
    }
private:
    T density;
};

template<typename T, template<typename U> class Descriptor>
class ApplyStentForce1_2D : public BoxProcessingFunctional2D_L<T,Descriptor> {
public:
    ApplyStentForce1_2D( T temp_, T porosity_, T uMax_,
                         std::vector<plint> const& cx_, std::vector<plint> const& cy_, 
                         std::vector<T> const& sthe_, std::vector<T> const& cthe_,
                         std::vector<T> const& ratio_);
    virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice);
    virtual ApplyStentForce1_2D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
private:
    T temp, factor_a, factor_b, factor_ap, factor_bp, uMax;
    vector<plint> cx, cy;
    vector<T> sthe, cthe, ratio;
};

/* ************* Class ApplyStentForce1_2D ******************* */

template<typename T, template<typename U> class Descriptor>
ApplyStentForce1_2D<T,Descriptor>::ApplyStentForce1_2D (
         T temp_, T porosity_,T uMax_,
         std::vector<plint> const& cx_, std::vector<plint> const& cy_, 
         std::vector<T> const& sthe_, std::vector<T> const& cthe_,
         std::vector<T> const& ratio_)
    : temp( temp_ ),
      factor_a( 12.25*(1-porosity_)+41.32*pow((1-porosity_)/porosity_,2) ),
      factor_b( 1.41*(1-porosity_)+1.11*pow((1-porosity_)/porosity_,2) ),
      factor_ap( 7.68*porosity_*porosity_-8.842*porosity_+2.734),
      factor_bp( 1. ),
      uMax(uMax_),
      cx(cx_), cy(cy_), sthe(sthe_), cthe(cthe_), ratio( ratio_ )
{
    PLB_ASSERT( cx.size() == cy.size() );
    PLB_ASSERT( cx.size() == sthe.size() );
    PLB_ASSERT( cx.size() == cthe.size() );
}

template<typename T, template<typename U> class Descriptor>
void ApplyStentForce1_2D<T,Descriptor>::process (
        Box2D domain, BlockLattice2D<T,Descriptor>& lattice )
{
    Dot2D absOfs = lattice.getLocation();
    
    for (pluint i=0; i<cx.size(); ++i) {

        plint cx_local = cx[i] - absOfs.x;
        plint cy_local = cy[i] - absOfs.y;
        if (contained(cx_local, cy_local, domain)) {
            Cell<T,Descriptor>& cell = lattice.get(cx_local,cy_local);
	    
            Array<T,2> u; cell.computeVelocity(u);
            T uN = norm(u);
            T u_n = u[0]*cthe[i] + u[1]*sthe[i];
            T u_t = u[1]*cthe[i] - u[0]*sthe[i];
            T uPower = u_n*u_n;
	    T un_a = abs(u_n);
            T re_n = temp*un_a; 
            T f_n=0., f_t=0.;

	    if (re_n > 0.1) {
	        //T drag_c = (factor_a / re_n + factor_b) * 0.5 * (1. + uN/un_a);
	        T drag_c = (factor_a / re_n + factor_b);
       	        f_n = -ratio[i] * 0.5 * drag_c * un_a * u_n;
                T B = 1.+drag_c/3.4 - pow(pow(drag_c/3.4,3.)+0.797, 1./3.);
                T vrc = 1. + re_n*factor_ap - pow( pow(re_n*factor_ap,1.5)+factor_bp, 2./3. );		
                f_t = -ratio[i] * u_t * un_a * B / vrc;
            }
            else {
                  f_n = -ratio[i] * (0.4799*un_a+0.007) * u_n;
                  f_t = -ratio[i] * 0.0174 * u_t;
  
             }

            T fx = f_n*cthe[i] - f_t*sthe[i];
            T fy = f_n*sthe[i] + f_t*cthe[i];

	     
            *cell.getExternal(Descriptor<T>::ExternalField::forceBeginsAt+0) = fx;
            *cell.getExternal(Descriptor<T>::ExternalField::forceBeginsAt+1) = fy;
        }
    }

}



template<typename T, template<typename U> class Descriptor>
ApplyStentForce1_2D<T,Descriptor>*
    ApplyStentForce1_2D<T,Descriptor>::clone() const
{
    return new ApplyStentForce1_2D<T,Descriptor>(*this);
}

template<typename T, template<typename U> class Descriptor>
void ApplyStentForce1_2D<T,Descriptor>::getTypeOfModification (
        std::vector<modif::ModifT>& modified ) const
{
    modified[0] = modif::staticVariables;
}


template<typename T, template<class U> class Descriptor>
void applyStentForce1 (
        MultiBlockLattice2D<T,Descriptor>& lattice, Box2D domain,
        T temp_, T porosity_, T uMax_,
        std::vector<plint> const& cx_, std::vector<plint> const& cy_, 
        std::vector<T> const& sthe_, std::vector<T> const& cthe_,
        std::vector<T> const& ratio_ )
{
    applyProcessingFunctional (
            new ApplyStentForce1_2D<T,Descriptor>(temp_, porosity_, uMax_, cx_, cy_, sthe_, cthe_, ratio_),
            domain, lattice );
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//assign parameters for the stent
void assignParameterStent( MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
                             IncomprFlowParam<T> const& parameters,T& porosity_,T& temp_,
			     T& uMax_, vector<plint>& cx_, vector<plint>& cy_,
			     vector<T>& sthe_, vector<T>& cthe_, vector<T>& ratio_)
{

    // radius should be propotional to the radius of cylinders in the fully resolved simulation.
    // The ratio is decided by the resolutions of these two simulation
        
    const plint nx = parameters.getNx();
    const plint ny = parameters.getNy();
    
    T radius = 0.5;
    T c_distance = parameters.getResolution();;
    T re = parameters.getRe()*2.*radius/c_distance;  
    T pi = acos(-1.); 
    porosity_ = 0.8;
    temp_ = re/parameters.getLatticeU(); 
//    ratio_ = pi/2.;// the ratio between the perameter of screen and ny
    uMax_ = parameters.getLatticeU();  

    
    //curve
    plint stentCxLb = (plint)(stentCx/dx + 0.5);
    plint stentCyLb = (plint)(stentCy/dx + 0.5);
    plint stentRLb = (plint)(stentR/dx + 0.5);
    plint stentLengthLb = (plint)(stentHeight/dx + 0.5);
    plint layer = 2;
        
    T sthe_c, cthe_c,ratio_c;
    plint cx, cy, i, n;
       
//     for (n = 0; n<layer; n++){
//         stentRLb -= n;        
        cx_.push_back(stentCxLb - stentRLb);
        cy_.push_back(stentCyLb);
        sthe_.push_back(0.);
        cthe_.push_back(-1.);
        ratio_.push_back(1.);
 
        for(i=1; i<stentLengthLb; i++){
            cy = stentCyLb + i;
            cx = stentCxLb - (plint)(sqrt(stentRLb*stentRLb-i*i) + 0.5);
            sthe_c = (T)i/(T)stentRLb;
            cthe_c = (T)(cx - stentCxLb)/(T)stentRLb;
            cx_.push_back(cx);
            cy_.push_back(cy);
            sthe_.push_back(sthe_c);
            cthe_.push_back(cthe_c);
            ratio_c = sqrt(1. + (cx_[i] - cx_[i-1])*(cx_[i] - cx_[i-1]));
            ratio_.push_back(ratio_c);        
       }
//     }
    
         
    return;
	 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////




/// boundary
void defineBoundary( MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
                             IncomprFlowParam<T> const& parameters,
                             OnLatticeBoundaryCondition2D<T,DESCRIPTOR>& boundaryCondition)
{    
    const plint nx = parameters.getNx();
    const plint ny = parameters.getNy();
    
    //inlet
    plint inletX0 = (plint)((Part2_tube) / dx + 0.5);
    plint inletX1 = (plint)((Part2_tube + l) / dx + 0.5);
    Box2D inlet(inletX0, inletX1, 0, 0);
    //outletA
    plint outletAY0 = (plint)((Part1_tube)/dx + 0.5);
    plint outletAY1 = (plint)((Part1_tube + l)/dx + 0.5);
    Box2D outletA(0, 0, outletAY0, outletAY1);

    //outletB
    plint outletBX = nx - 1;
    plint outletBY0 = (plint)((Part1_tube + Part3_R1)/dx + 0.5);
    plint outletBY1 = (plint)((Part1_tube + Part3_R2)/dx + 0.5);
    Box2D outletB(outletBX, outletBX, outletBY0, outletBY1);


    // Create Velocity boundary conditions on inlet Box
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, inlet);
    // .. except on right boundary, where we prefer a fixed-pressure condition.
    boundaryCondition.setPressureConditionOnBlockBoundaries(lattice, outletA);
//     boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, outlet);
    boundaryCondition.setPressureConditionOnBlockBoundaries(lattice, outletB);

  
    setBoundaryVelocity (
            lattice, inlet,
             PoiseuilleVelocity<T>(parameters, inletX0, inletX1, 0) );
//             Array<T,2>(0.,parameters.getLatticeU()) );
    
    setBoundaryDensity (
            lattice, outletA,
            ConstantDensity<T>(1.) );
    
    setBoundaryDensity (
            lattice, outletB,
            ConstantDensity<T>(1.) );
    
    
    initializeAtEquilibrium (
            lattice, lattice.getBoundingBox(),
            1., Array<T,2>(0., 0.) );  
    
    lattice.initialize();
    
    
}

//==========================================================================================
// The function used for set the values of positons and normals for the calculation of wss
void wallPosition(plint cx_,plint cy_,T radius_,std::vector<Array<T,2> >& positions,
                  std::vector<Array<T,2> >& normals)
{
    
    plint cx(cx_);
    plint cy(cy_);
    T radius(radius_ - 1.);
    Array<T,2> position, normal;
    plint iX, iY1, iY2, cyLow;
    T sinValue, cosValue;
    cyLow = (plint)((Part1_tube + l)/dx + 0.5) - 1;
    
    for(iX=cx-radius; iX<=cx+radius; iX++){
        iY1 = (plint)(sqrt(radius*radius - (iX-cx)*(iX-cx))) + cy;
        iY2 = (plint)(sqrt(radius*radius - (iX-cx)*(iX-cx))) * (-1) + cy;
        
        position = {iX,iY1};
        positions.push_back(position);        
        sinValue = (T(iY1-cy))/radius;
        cosValue = (T(iX-cx))/radius;
        normal = {cosValue,sinValue};
        normals.push_back(normal); 
        
        if(iY2 >= cyLow){
           position = {iX,iY2};
           positions.push_back(position);        
           sinValue = (T(iY2-cy))/radius;
           cosValue = (T(iX-cx))/radius;
           normal = {cosValue,sinValue};
           normals.push_back(normal); 
        }
    }
    
    pcout << "cx = " << cx << "; cy = " << cy;
    pcout << "radius = " << radius << "; cyLow = " << cyLow << endl;
}
    
void writeGifs(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter)
{
    const plint imSize = 600;

    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledGif(createFileName("u", iter, 8),
                               *computeVelocityNorm(lattice),
                               imSize, imSize );
}

void writeVTK(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
              IncomprFlowParam<T> const& parameters, plint iter)
{
    T dx = parameters.getDeltaX();
    T dt = parameters.getDeltaT();
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 8), dx);
    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", dx/dt);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", dx/dt);
}

void writeTextFile(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter) {
    plb_ofstream pfile((createFileName("pressure", iter, 8)+".txt").c_str());
    pfile << *multiply(1./3., *add(-1.0, *computeDensity(lattice)));
    
    plb_ofstream ufile((createFileName("velocity", iter, 8)+".txt").c_str());
    ufile << *computeVelocity(lattice);
    
    plb_ofstream sfile((createFileName("stress", iter, 8)+".txt").c_str());
    sfile << *computeStrainRateFromStress(lattice);
    
}

void wssFile(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,std::vector<Array<T,2> >& positions,
             std::vector<Array<T,2> >& normals, plint iter){
    
     vector<T> wssMeasure;
     wssMeasure = wssSingleProbes(lattice, positions, normals);
     plb_ofstream wfile((createFileName("wss", iter, 8)+".txt").c_str());
     for (pluint i=0; i<positions.size(); ++i) {
          wfile << i << "\t" << wssMeasure[i] << endl;
     }
}                     

int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");

    IncomprFlowParam<T> parameters(
            (T) 0.0547,  // uMax
            (T) 400.,  // Re
            90,        // N
	    lx, ly );

    const T logT     = (T)0.1;
    const T imSave   = (T)0.5;
    const T vtkSave  = (T)30;
    const T textSave = (T)300.;
    const T maxT     = (T)600.;
    const plint checkPointIter = parameters.nStep(100.);
    plint iniT, endT;
    pcout << "the maximux iteration is: " << parameters.nStep(maxT) << endl;

    if (argc != 3) {
        pcout << "Error; the syntax is \"" << argv[0] << " start-iter end-iter\"," << endl;
        return -1;
    }

    stringstream iniTstr, endTstr;
    iniTstr << argv[1]; iniTstr >> iniT;
    endTstr << argv[2]; endTstr >> endT;

    writeLogFile(parameters, "Poiseuille flow");
    ///*******************************************************************************************
    plint nx = parameters.getNx();
    plint ny = parameters.getNy();
    MultiBlockLattice2D<T, DESCRIPTOR> lattice (
            nx, ny, new BounceBack<T,DESCRIPTOR> );
    dx = parameters.getDeltaX();
    
    //Part1
    plint X0 = (plint)((Part2_tube)/dx + 0.5);
    plint X1 = (plint)((Part2_tube + l)/dx + 0.5);
    plint Y0 = 0;
    plint Y1 = (plint)((Part1_tube + l)/dx + 0.5);
    Box2D part1(X0, X1, Y0, Y1);
    defineDynamics(lattice, part1,
               // new NaiveExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               //new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    
    //Part2
    X0 = 0;
    X1 = (plint)((Part2_tube + l)/dx + 0.5);
    Y0 = (plint)((Part1_tube)/dx + 0.5);
    Y1 = (plint)((Part1_tube + l)/dx + 0.5);
    Box2D part2(X0, X1, Y0, Y1);
    defineDynamics(lattice, part2,
               // new NaiveExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               //new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    

    //Part3
    //Part3_circle
    plint CxLb = (plint)(Part3_Cx/dx + 0.5);
    plint CyLb = (plint)(Part3_Cy/dx + 0.5);
    plint Part3_R1Lb = (plint)(Part3_R1/dx + 0.5);
    plint Part3_R2Lb = (plint)(Part3_R2/dx + 0.5);
    
    defineDynamics(lattice, lattice.getBoundingBox(),
               new QuarterCircleLeftDomain2D<T>( CxLb, CyLb, Part3_R2Lb),
               //new NaiveExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               //new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    defineDynamics(lattice, lattice.getBoundingBox(),
               new QuarterCircleLeftDomain2D<T>( CxLb, CyLb, Part3_R1Lb),
               new BounceBack<T,DESCRIPTOR> );
    //Part3_tube
    X0 = (plint)(Part3_Cx/dx);
    X1 = nx;
    Y0 = (plint)((Part3_Cy+Part3_R1)/dx + 0.5);
    Y1 = (plint)((Part3_Cy+Part3_R2)/dx + 0.5);
    Box2D part3Tube(X0, X1, Y0, Y1);
    defineDynamics(lattice, part3Tube,
               // new NaiveExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               //new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );

    //Aneurysm
    plint aneurysmCxLb = (plint)(aneurysmCx / parameters.getDeltaX() + 0.5);
    plint aneurysmCyLb = (plint)(aneurysmCy / parameters.getDeltaX() + 0.5);
    plint aneurysmRadiusLb = (plint)(aneurysmRadius / parameters.getDeltaX() + 0.5);
    
    std::vector<plb::plint> cxlist, cylist;
    cxlist.push_back(aneurysmCxLb);
    cylist.push_back(aneurysmCyLb);
    defineDynamics(lattice, lattice.getBoundingBox(),
               //new HalfCircleDomain2D<T>( aneurysmCxLb,aneurysmCyLb, aneurysmRadiusLb),
               new AngleCylinderShapeDomain2D<T>(cxlist,cylist,aneurysmRadiusLb, 0),
               //new NaiveExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));
               new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(parameters.getOmega()));

   
    lattice.initialize();
    ///*******************************************************************************************
     
//     // The drag and lift acting on the obstacle are computed with help of the
//     //   internal statistics object of the lattice. For this purpose, they
//     //   need to be registered first, as it is done in the following lines. 
    OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
        boundaryCondition = createInterpBoundaryCondition2D<T,DESCRIPTOR>();
        //boundaryCondition = createLocalBoundaryCondition2D<T,DESCRIPTOR>();

    defineBoundary(lattice, parameters, *boundaryCondition);   
    
    //***********************************************************************************************
    T porosity; // The porosity of stent
    T temp; //temp=Re_d/uLB=mu/rou/d
    // when the stent is incline, the force implied on one node should be ratio*1>1 node.
    // Because when the straight line is bend, it is elongated. So the force on node shoud be
    // for the length of curve through one node.
    T uMax;
    vector<plint> cx,cy;
    vector<T> sthe, cthe, ratio;
    
    assignParameterStent(lattice,parameters,porosity,temp,uMax, cx, cy, sthe,cthe, ratio);
    //***********************************************************************************************
    //set the postion of the wall on which the wss would be measured
    vector<Array<T,2> > positions, normals;
    wallPosition(aneurysmCxLb,aneurysmCyLb,aneurysmRadiusLb,positions,normals);
    pcout << "number of wall points: " << positions.size() << endl;

//    //****************************************************************************//
//        //spongeZone
//    Array<plint,4> numSpongeCells;
//    numSpongeCells[0] = 0;
//    numSpongeCells[1] = 0;
//    numSpongeCells[2] = ny/4;
//    numSpongeCells[3] = 0;
//    Array<T,4> translationParameters(0.5, 0.5, 0.5, 0.5);
//    Array<T,4> scaleParameters(0.12, 0.12, 0.12, 0.12);
//    std::vector<MultiBlock2D *> args;
//    args.push_back(&lattice);
////     plint box_x0 = (plint)((margin+R2+R1)/dx);
//    plint box_x0 = (plint)((margin+R2)/dx);
//    plint box_x1 = (plint)((margin+2*R2)/dx+1.0);
//    plint box_y0 = 0;
//    plint box_y1 = ny-1;
//    applyProcessingFunctional (
//            new ViscositySpongeZone2D<T,DESCRIPTOR> (
//               nx, ny, parameters.getOmega(), numSpongeCells, translationParameters, scaleParameters ),
//               rightTube, args );
//    
//    
    // Main loop over time iterations.
    if (iniT>0) {
        //loadRawMultiBlock(lattice, "checkpoint.dat");
        loadBinaryBlock(lattice, "checkpoint.dat");
    }
    
    global::PlbTimer time;
    time.start();
    plint iT=0;
    //for (; iT*parameters.getDeltaT()<maxT; ++iT) {  
    for (iT=iniT; iT<endT; ++iT) {
       
        plint begForce = 1e+5;
        plint interForce = 1e+4;
        T susRel;
       
        if (iT > begForce){
//            if(iT - begForce < interForce)
//                susRel = (iT - begForce) / (T)interForce;
//            else
//                susRel = 1.;
           applyStentForce1 (
           lattice, lattice.getBoundingBox(),
           temp, porosity, uMax, cx, cy, sthe, cthe, ratio);
        }
    
//       if (iT%parameters.nStep(imSave)==0) {
//           pcout << "Saving Gif ..." << endl;
//           writeGifs(lattice, iT);
//       }
//        
        if (iT%parameters.nStep(vtkSave)==0 && iT>0) {
            pcout << "Saving VTK file ..." << endl;
            writeVTK(lattice, parameters, iT);

        }
        
	if (iT%parameters.nStep(textSave)==0 && iT>0) {
           pcout << "Saving text file ..." << endl;
	    writeTextFile(lattice, iT);
            wssFile(lattice, positions, normals, iT);
       }
        
        if (iT%checkPointIter==0 && iT>iniT) {
            pcout << "Saving the state of the simulation ..." << endl;
            //saveRawMultiBlock(lattice, "checkpoint.dat");
            saveBinaryBlock(lattice, "checkpoint.dat");
        }
        
        if (iT%parameters.nStep(logT)==0) {
            pcout << "step " << iT
                  << "; lattice time=" << lattice.getTimeCounter().getTime()
                  << "; t=" << iT*parameters.getDeltaT()
                  << "; time=" << time.getTime();;
        }

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();

        if (iT%parameters.nStep(logT)==0) {
            pcout << "; av energy="
                  << setprecision(10) << getStoredAverageEnergy<T>(lattice)
                  << "; av rho="
                  << getStoredAverageDensity<T>(lattice)
                  << endl;           
		    
	            
        }

    }
    
    writeTextFile(lattice, iT); 
    wssFile(lattice, positions, normals, iT);
    pcout << "Saving Pressure file ..." << endl;
    pcout << "Saving Velocity file ..." << endl;
    pcout << "Saving The Strain Rate From Stress file ..." << endl;
    pcout << "Saving The Wss file ..." << endl;

    delete boundaryCondition;
}
