/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2012 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTICYLINDER_H
#define MULTICYLINDER_H

#include "palabos2D.h"

template<typename T>
class MultiCylinderShapeDomain2D : public plb::DomainFunctional2D {
public:
    MultiCylinderShapeDomain2D(plb::plint cx_, std::vector<plb::plint> cy_, plb::plint radius)
        : cx(cx_),
          cy(cy_),
          radiusSqr(plb::util::sqr(radius))
    {}
    virtual bool operator() (plb::plint iX, plb::plint iY) const {
       plb::plint n = cy.size();
       for(plb::plint i=0;i<n;i++){
	 if(plb::util::sqr(iX-cx) + plb::util::sqr(iY-cy[i]) <= radiusSqr){
	    return true;
	 }
       }
       return false;
    }
    
    virtual MultiCylinderShapeDomain2D<T>* clone() const {
        return new MultiCylinderShapeDomain2D<T>(*this);
    }
private:
    plb::plint cx;
    std::vector<plb::plint> cy;
    plb::plint radiusSqr;
};

template<typename T>
class OneCylinderShapeDomain2D : public plb::DomainFunctional2D {
public:
    OneCylinderShapeDomain2D(plb::plint cx_, std::vector<plb::plint> cy_, plb::plint radius, plb::plint i_)
        : cx(cx_),
          cy(cy_),
          radiusSqr(plb::util::sqr(radius)),
          i(i_)
    {}
    virtual bool operator() (plb::plint iX, plb::plint iY) const {
     	 if(plb::util::sqr(iX-cx) + plb::util::sqr(iY-cy[i]) <= radiusSqr){
	    return true;
	 }       
       return false;
    }
    
    virtual OneCylinderShapeDomain2D<T>* clone() const {
        return new OneCylinderShapeDomain2D<T>(*this);
    }
private:
    plb::plint cx;
    std::vector<plb::plint> cy;
    plb::plint radiusSqr;
    plb::plint i;
};

template<typename T>
class AngleCylinderShapeDomain2D : public plb::DomainFunctional2D {
public:
    AngleCylinderShapeDomain2D(std::vector<plb::plint> cx_, std::vector<plb::plint> cy_, plb::plint radius, plb::plint i_)
        : cx(cx_),
          cy(cy_),
          radiusSqr(plb::util::sqr(radius)),
          i(i_)
    {}
    virtual bool operator() (plb::plint iX, plb::plint iY) const {
     	 if(plb::util::sqr(iX-cx[i]) + plb::util::sqr(iY-cy[i]) <= radiusSqr){
	    return true;
	 }       
       return false;
    }
    
    virtual AngleCylinderShapeDomain2D<T>* clone() const {
        return new AngleCylinderShapeDomain2D<T>(*this);
    }
private:
    std::vector<plb::plint> cx;
    std::vector<plb::plint> cy;
    plb::plint radiusSqr;
    plb::plint i;
};

/// Half circle
template<typename T>
class HalfCircleDomain2D : public plb::DomainFunctional2D {
public:
  HalfCircleDomain2D(plb::plint cxLb_, plb::plint cyLb_, plb::plint radiusLb_)
  : cxLb(cxLb_),
    cyLb(cyLb_),
    radiusLb(radiusLb_)
    {}
    virtual bool operator() (plb::plint iX, plb::plint iY) const {
     	 if ( 
                 (iY >= cyLb) && 
                 (plb::util::sqr(iX-cxLb) + plb::util::sqr(iY-cyLb) <= plb::util::sqr(radiusLb)) )
         {
	    return true;
	 }       
       return false;
    }
    
    virtual HalfCircleDomain2D<T>* clone() const {
        return new HalfCircleDomain2D<T>(*this);
    }
private:
  plb::plint cxLb;
  plb::plint cyLb;
  plb::plint radiusLb;  
};

/// left quarter circle
template<typename T>
class QuarterCircleLeftDomain2D : public plb::DomainFunctional2D {
public:
  QuarterCircleLeftDomain2D(plb::plint cxLb_, plb::plint cyLb_, plb::plint radiusLb_)
  : cxLb(cxLb_),
    cyLb(cyLb_),
    radiusLb(radiusLb_)
    {}
    virtual bool operator() (plb::plint iX, plb::plint iY) const {
     	 if ( 
                 (iX <= cxLb) && (iY >= cyLb) && 
                 (plb::util::sqr(iX-cxLb) + plb::util::sqr(iY-cyLb) <= plb::util::sqr(radiusLb)) )
         {
	    return true;
	 }       
       return false;
    }
    
    virtual QuarterCircleLeftDomain2D<T>* clone() const {
        return new QuarterCircleLeftDomain2D<T>(*this);
    }
private:
  plb::plint cxLb;
  plb::plint cyLb;
  plb::plint radiusLb;  
};

/// quarter circle
template<typename T>
class QuarterCircleRightDomain2D : public plb::DomainFunctional2D {
public:
  QuarterCircleRightDomain2D(plb::plint cxLb_, plb::plint cyLb_, plb::plint radiusLb_)
  : cxLb(cxLb_),
    cyLb(cyLb_),
    radiusLb(radiusLb_)
    {}
    virtual bool operator() (plb::plint iX, plb::plint iY) const {
     	 if ( 
                 (iX >= cxLb) && (iY >= cyLb) && 
                 (plb::util::sqr(iX-cxLb) + plb::util::sqr(iY-cyLb) <= plb::util::sqr(radiusLb)) )
         {
	    return true;
	 }       
       return false;
    }
    
    virtual QuarterCircleRightDomain2D<T>* clone() const {
        return new QuarterCircleRightDomain2D<T>(*this);
    }
private:
  plb::plint cxLb;
  plb::plint cyLb;
  plb::plint radiusLb;  
};


/// Convert pressure to density according to ideal gas law
template<typename T, template<typename U> class Descriptor>
void createCylinder( plb::MultiBlockLattice2D<T,Descriptor>& lattice,
                     std::vector<plb::plint> cx,  std::vector<plb::plint> cy, plb::plint radius, plb::plint i);

#endif  // CYLINDER_H
