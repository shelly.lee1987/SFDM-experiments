/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2012 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "palabos2D.h"
#include "palabos2D.hh"
#include "cylinder.h"
#include "cylinder.hh"
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace plb;
using namespace plb::descriptors;
using namespace std;

typedef double T;
#define DESCRIPTOR D2Q9Descriptor

/// A functional, used to initialize a pressure boundary to constant density
template<typename T>
class ConstantDensity {
public:
    ConstantDensity(T density_)
        : density(density_)
    { }
    T operator()(plint iX, plint iY) const {
        return density;
    }
private:
    T density;
};


/// A functional, used to instantiate bounce-back nodes at the locations of the cylinder
void defineCylinderGeometry( MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
                             IncomprFlowParam<T> const& parameters,
                             OnLatticeBoundaryCondition2D<T,DESCRIPTOR>& boundaryCondition,
                             Array<plint,2> forceIds )
{
    const plint nx = parameters.getNx();
    const plint ny = parameters.getNy();
    //Box2D everythingButOutlet(0, nx-2, 0, ny-1);
    Box2D inlet(0,0, 0, ny-1);
    Box2D outlet(nx-1,nx-1, 0, ny-1);
    
    lattice.periodicity().toggleAll(true); // Use periodic boundaries.

    // Create Velocity boundary conditions for inlet
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, inlet);
    // right boundary, where we prefer a fixed-pressure condition.
    boundaryCondition.setPressureConditionOnBlockBoundaries(lattice, outlet);
   // boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, outlet);
   

    
    
    // All cells have initially density rho ... 
    T rho0=1.;
    T u = parameters.getLatticeU();
    T theta = 30.;
    T pi = acos(-1.);
    T ux = u * cos(theta*pi/180.);
    T uy = u * sin(theta*pi/180.);
    
    setBoundaryVelocity (
            lattice, inlet,
            Array<T,2>(ux,uy) );
    setBoundaryDensity (
            lattice, outlet,
            ConstantDensity<T>(1.) );
    //setBoundaryVelocity (
      //      lattice, outlet,
        //    Array<T,2>(ux,uy) );
    
    initializeAtEquilibrium (
            lattice, lattice.getBoundingBox(),
            rho0, Array<T,2>(ux,uy) );

    int cx     = 500;
    int cy     = ny/2;
    int radius = 40;
    // Instead of plain BounceBack, use the dynamics MomentumExchangeBounceBack,
    //   to compute the momentum exchange, and thus, the drag and the lift on
    //   the obstacle, locally during collision.
    defineDynamics(lattice, lattice.getBoundingBox(),
                   new CylinderShapeDomain2D<T>(cx,cy,radius),
                   new MomentumExchangeBounceBack<T,DESCRIPTOR>(forceIds));
    initializeMomentumExchange(lattice, lattice.getBoundingBox(), new CylinderShapeDomain2D<T>(cx, cy, radius) );

    lattice.initialize();
}

void writeGifs(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter)
{
    const plint imSize = 600;

    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledGif(createFileName("u", iter, 6),
                               *computeVelocityNorm(lattice),
                               imSize, imSize );
}

void writeVTK(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
              IncomprFlowParam<T> const& parameters, plint iter)
{
    T dx = parameters.getDeltaX();
    T dt = parameters.getDeltaT();
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 8), dx);
    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", dx/dt);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", dx/dt);
}

void writeTextFile(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter) {
    plb_ofstream pfile((createFileName("pressure", iter, 8)+".txt").c_str());
    pfile << *multiply(1./3., *add(-1.0, *computeDensity(lattice)));
    
    plb_ofstream ufile((createFileName("velocity", iter, 8)+".txt").c_str());
    ufile << *computeVelocity(lattice);
    
    plb_ofstream sfile((createFileName("stress", iter, 8)+".txt").c_str());
    sfile << *computeStrainRateFromStress(lattice);
}

int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");

    IncomprFlowParam<T> parameters(
            (T) 1e-3,  // uMax
            (T) 10,  // Re
            200,        // N
            6.,         // lx
            1.         // ly 
    );
        
    const T logT     = (T)0.01;
    const T imSave   = (T)50;
    const T vtkSave  = (T)5.;
    //const T textSave = (T)20.;
    const T maxT     = (T)50.1;

    writeLogFile(parameters, "Poiseuille flow");

    MultiBlockLattice2D<T, DESCRIPTOR> lattice (
            parameters.getNx(), parameters.getNy(),
            new BGKdynamics<T,DESCRIPTOR>(parameters.getOmega()) );
    lattice.initialize();

    // The drag and lift acting on the obstacle are computed with help of the
    //   internal statistics object of the lattice. For this purpose, they
    //   need to be registered first, as it is done in the following lines.
    Array<plint,2> forceIds;
    forceIds[0] = lattice.internalStatSubscription().subscribeSum();
    forceIds[1] = lattice.internalStatSubscription().subscribeSum();

    OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
        boundaryCondition = createInterpBoundaryCondition2D<T,DESCRIPTOR>();
        //boundaryCondition = createLocalBoundaryCondition2D<T,DESCRIPTOR>();

    defineCylinderGeometry(lattice, parameters, *boundaryCondition, forceIds);

    // Main loop over time iterations.
    
    FILE *fp_force = NULL;
    
    if (global::mpi().isMainProcessor()) {
      fp_force = fopen("force.dat","w+");
      PLB_ASSERT(fp_force!=NULL);
    }
    
    T aforce=0.;
    T bforce=0.;
    plint iT=0;
    //for (; iT*parameters.getDeltaT()<maxT; ++iT) {
    do{
     
        //global::timer("mainLoop").restart();
	      	
        if (iT%parameters.nStep(imSave)==0) {
            pcout << "Saving Gif ..." << endl;
            writeGifs(lattice, iT);
        }
        
        if (iT%parameters.nStep(vtkSave)==0 && iT>0) {
            pcout << "Saving VTK file ..." << endl;
            writeVTK(lattice, parameters, iT);
        }

        //if (iT%parameters.nStep(textSave)==0 && iT>0) {
           // pcout << "Saving text file ..." << endl;
	    //writeTextFile(lattice, iT);
        //}

        if (iT%parameters.nStep(logT)==0) {
	    aforce= bforce; 
            pcout << "step " << iT
                  << "; lattice time=" << lattice.getTimeCounter().getTime()
                  << "; t=" << iT*parameters.getDeltaT();
        }

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();

        if (iT%parameters.nStep(logT)==0) {
	    
            pcout << "; av energy="
                  << setprecision(10) << getStoredAverageEnergy<T>(lattice)
                  << "; av rho="
                  << getStoredAverageDensity<T>(lattice)
                  << "; drag=" << lattice.getInternalStatistics().getSum(forceIds[0])
                  << "; lift=" << lattice.getInternalStatistics().getSum(forceIds[1])
                  << endl;
		  
		  if(global::mpi().isMainProcessor()){
                   fprintf(fp_force, "% .8e\t% .8e\t% .8e\n", 
	                          (T) iT*parameters.getDeltaT(), 
			          (T) lattice.getInternalStatistics().getSum(forceIds[0]), 
			          (T) lattice.getInternalStatistics().getSum(forceIds[1]));			
                   fflush(fp_force);
		}
	bforce=lattice.getInternalStatistics().getSum(forceIds[0]); 
		
        }  
        
   
    ++iT;
    }while( iT*parameters.getDeltaT()<maxT || fabs((bforce-aforce)/bforce)>=1e-9);
   
    
    writeTextFile(lattice, iT); 
    pcout << "Saving Pressure file ..." << endl;
    pcout << "Saving Velocity file ..." << endl;
    pcout << "Saving The Strain Rate From Stress file ..." << endl;
    	   

    delete boundaryCondition;
}
